package it.ied.ruffolo.eventi.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.tabs.TabLayout
import it.ied.ruffolo.eventi.models.Event
import kotlinx.android.synthetic.main.layout_tabbar.*
import java.text.SimpleDateFormat

abstract class BaseEventsActivity : AppCompatActivity()
{
    var events: MutableList<Event> = mutableListOf()

    abstract fun getActivityLayout(): Int

    open fun setupActivityData()
    {
        //Questa funzione va inplementata nelle sottoclassi che vogliono inizializzare la schermata
        //Creo alcuni eventi di prova
        /*events.add(
            Event(
                name = "Evento Musica",
                description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ",
                price = 50.0,
                date = SimpleDateFormat("dd/MM/yyyy HH:mm").parse("05/10/2020 9:30"),
                address = "Indirizzo di prova",
                imageUrl = "https://images.unsplash.com/photo-1593934315159-dced6b067412?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1503&q=80",
                latitude = 45.0,
                longitude = 10.0
            )
        )
        events.add(
            Event(
                name = "Evento Due ",
                price = 80.0,
                date = SimpleDateFormat("dd/MM/yyyy HH:mm").parse("10/09/2020 12:30"),
                address = "Indirizzo di prova",
                imageUrl = "https://images.unsplash.com/photo-1578839509567-6425efb1ce8e?ixlib=rb-1.2.1&auto=format&fit=crop&w=1352&q=80",
                latitude = 30.0,
                longitude = 35.0
            )
        )
        events.add(
            Event(
                name = "Evento Tre",
                price = 50.0,
                address = "Indirizzo",
                latitude = 143.0,
                longitude = 79.0
            )
        )
        events.add(
            Event(
                name = "Evento Quattro",
                address = "Indirizzo",
                latitude = 400.0,
                longitude = 30.0
            )

        )*/

        //Inizializzo la Tab bar
        tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                selectedTabChanged(tab?.position ?: 0)

            }
            override fun onTabReselected(tab: TabLayout.Tab?) {
                selectedTabChanged(tab?.position ?: 0)
            }

            override fun onTabUnselected(tab: TabLayout.Tab?)
            {

            }
        })
    }


    open fun updateActivityData()
    {

    }

    internal open fun selectedTabChanged(index: Int) {
        //Funzione che viene richiamata quando viene selezionato un TabItem
        when(index) {
            0 -> {
                if (this is HomeActivity) {
                    //Non apro sempre la stessa pagina
                } else {
                    //E' stata selezionata la schermata Home
                    val intent = Intent(applicationContext, HomeActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
            1 -> {
                if (this is EventsMapActivity) {
                    //Non apro sempre la stessa pagina
                } else {
                    //E' stata selezionata la schermata della Mappa
                    val intent = Intent(applicationContext, EventsMapActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
            2 -> {
                if (this is ProfileActivity) {
                    //Non apro sempre la stessa pagina
                } else {
                    //E' stata selezionata la schermata del profilo
                    val intent = Intent(applicationContext, ProfileActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getActivityLayout())
        setupActivityData()
        updateActivityData()
    }
}







