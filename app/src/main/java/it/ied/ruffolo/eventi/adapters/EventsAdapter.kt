package it.ied.ruffolo.eventi.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import it.ied.ruffolo.eventi.models.Event
import it.ied.ruffolo.eventi.R
import kotlinx.android.synthetic.main.listitem_event.view.*
import java.text.SimpleDateFormat
import java.util.*

typealias EventCallback = (Event)  -> Unit

class EventsAdapter(
    var items: List<Event>,
    var contect: Context
) : RecyclerView.Adapter<ViewHolder>() {

    var onEventClickListener: OnEventClickListener? = null
    var onEventClickCallback: EventCallback? = null


    fun setEventsList(eventsList: List<Event>){
        items = eventsList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(contect).inflate(R.layout.listitem_event, parent, false)
        return ViewHolder(view)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val event = items[position]

        val dataEvento = event.date ?: Date()
        holder.textNomeGiorno.text = SimpleDateFormat("EEEE").format(dataEvento)
        holder.textNumeroGiorno.text = SimpleDateFormat("dd").format(dataEvento)

        holder.textName.text = event.name
        holder.textAddress.text = event.address

        if(event.imageUrl != null){
            Picasso.get().load(event.imageUrl).fit().centerCrop().into(holder.imageBanner)
        }

        holder.itemView.setOnClickListener{
            onEventClickListener?.OnEventClick(event)
            onEventClickCallback?.invoke(event)
        }

    }
}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
    val textNomeGiorno = view.text_nome_giorno
    val textNumeroGiorno = view.text_numero_giorno

    val textName = view.text_nome
    val textAddress = view.text_address

    val imageBanner = view.image_banner

}

interface OnEventClickListener {
    fun OnEventClick(event: Event)
}