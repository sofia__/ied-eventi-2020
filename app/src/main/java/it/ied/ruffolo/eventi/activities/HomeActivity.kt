package it.ied.ruffolo.eventi.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import it.ied.ruffolo.eventi.R
import it.ied.ruffolo.eventi.fragments.*

class HomeActivity : BaseEventsActivity() {

    //variabile di appoggio per interagire con il fragment attualmente visibile
    private var mCurrentFragment: BaseFragment?= null

    override fun getActivityLayout(): Int {
        return R.layout.activity_home
    }

    override fun setupActivityData() {
        super .setupActivityData()

        val fragment = EventsPagerFragment()
        fragment.events = this.events
        replaceCurrentFragment(fragment)
    }

    private fun replaceCurrentFragment(with: BaseFragment){
        supportFragmentManager.beginTransaction().replace(R.id.main_container, with).commit()
        mCurrentFragment = with
    }

    override fun selectedTabChanged(index: Int) {
        when(index) {
            0 -> {
                if (this is HomeActivity) {
                    //Non apro sempre la stessa pagina
                } else {
                    //E' stata selezionata la schermata Home
                    val intent = Intent(applicationContext, HomeActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
            1 -> {
               //per adesso non faccio nulla
            }
            2 -> {
                if (mCurrentFragment is ProfileFragment) {
                    //Non apro sempre la stessa pagina
                } else {
                    //E' stata selezionata la schermata del profilo
                    val fragment = ProfileFragment()
                    replaceCurrentFragment(fragment)
                }
            }
        }
    }



}