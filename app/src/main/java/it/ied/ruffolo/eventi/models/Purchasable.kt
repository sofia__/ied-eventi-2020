package it.ied.ruffolo.eventi.models

import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@Parcelize
open class Purchasable(

    open var id: Int? = null,

    open var name: String? = null,
    open var description: String? = null,
    open var imageUrl: String? = null,

    open var quantity : Int = 0,
    open var price: Double = 0.0

) : Parcelable {
}