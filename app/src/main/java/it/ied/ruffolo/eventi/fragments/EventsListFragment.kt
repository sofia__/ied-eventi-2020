package it.ied.ruffolo.eventi.fragments

import android.content.Intent
import android.os.Bundle
import android.renderscript.Sampler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import it.ied.ruffolo.eventi.R
import it.ied.ruffolo.eventi.activities.EventDetailActivity
import it.ied.ruffolo.eventi.adapters.EventsAdapter
import it.ied.ruffolo.eventi.models.Event
import kotlinx.android.synthetic.main.activity_events.*


class EventsListFragment : BaseFragment () {

    var events: List<Event> = ArrayList()
        set(value) {
            field = value
            //passo la lista di eventi aggiornata all'adapter della RecyclerView
            (recycler_view?.adapter as? EventsAdapter)?.setEventsList(value)
        }

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_events_list
    }

    override fun setupFragmentData() {
        super.setupFragmentData()

        recycler_view.layoutManager = LinearLayoutManager(activity)
        val adapter = activity?.let { EventsAdapter(events, it) }
        adapter?.onEventClickCallback = { event ->
            val intent = Intent(activity, EventDetailActivity::class.java)

            intent.putExtra("event", event)

            startActivity(intent)
        }
        recycler_view.adapter = adapter
    }
}