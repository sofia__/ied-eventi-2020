package it.ied.ruffolo.eventi.fragments

import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.tabs.TabLayoutMediator
import it.ied.ruffolo.eventi.R
import it.ied.ruffolo.eventi.activities.HomeActivity
import it.ied.ruffolo.eventi.adapters.EventsAdapter
import it.ied.ruffolo.eventi.adapters.EventsPagerAdapter
import it.ied.ruffolo.eventi.helpers.AlertHelper
import it.ied.ruffolo.eventi.helpers.NetworkParser
import it.ied.ruffolo.eventi.models.Event
import kotlinx.android.synthetic.main.activity_events.*
import kotlinx.android.synthetic.main.fragment_events_pager.*

class EventsPagerFragment : BaseFragment() {

    var events: List<Event> = ArrayList()

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_events_pager
    }

    override fun setupFragmentData() {
        super.setupFragmentData()

        val adapter = activity?.let { EventsPagerAdapter(it) }
        adapter?.events = this.events
        view_pager.adapter = adapter

        TabLayoutMediator(tab_layout, view_pager){tab, position ->
            if ( position == 0){
                tab.text = "LISTA"
            }else{
               tab.text = "MAPPA"
            }

        }.attach()

        updateEvents()
    }

    fun updateEvents() {
        var url = "http://ied.apptoyou.it/app/api/eventi.php"

        val request = StringRequest(
            Request.Method.GET, url,
            { response ->
                val updatedEvents = NetworkParser.parseEventsList(response)

                (view_pager.adapter as? EventsPagerAdapter)?.events = updatedEvents

                //Aggiorno la lista globale degli eventi
                (activity as? HomeActivity)?.events = updatedEvents.toMutableList()


                //AlertHelper.showToast(activity,"Eventi caricati: ${updatedEvents.size}" )
            },
            {error ->

                AlertHelper.showToast(activity,"Si è verificato un errore")
            })

        val queue = Volley.newRequestQueue(activity)
        queue.add(request)
    }
}
