package it.ied.ruffolo.eventi.helpers

import it.ied.ruffolo.eventi.models.Purchasable

class ShoppingCartHelper {

    companion object {
        var items: MutableList<Purchasable> = mutableListOf()
        fun addItem(item: Purchasable) {
            if (!items.contains(item)) {
                items.add(item)
            }
        }
        fun clearCart() {
            items = mutableListOf()
        }
    }

}