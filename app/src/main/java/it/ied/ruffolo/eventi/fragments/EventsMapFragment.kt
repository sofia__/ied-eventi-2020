package it.ied.ruffolo.eventi.fragments

import android.content.Intent
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import it.ied.ruffolo.eventi.R
import it.ied.ruffolo.eventi.activities.EventDetailActivity
import it.ied.ruffolo.eventi.models.Event

class EventsMapFragment : BaseFragment(), OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {

    var events: List<Event> = ArrayList()
        set(value) {
            field = value

            //Passo la lista di eventi aggiornata alla funzione che li carica su GOOGLEMap
            reloadMarkers()
        }

    private lateinit var mMap: GoogleMap
    private var mIsMapReady = false


    override fun getFragmentLayout(): Int {
        return R.layout.fragment_events_map
    }

    override fun setupFragmentData() {
        super.setupFragmentData()

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun reloadMarkers(){
        if (!mIsMapReady){
            // la mappa non è ancora pronta
            return
        }
        //Elimino gli eventuali marker aggiunti precedentemente
        mMap.clear()

        for (event in events)
        {
            //Aggiungo il marker sulla mappa
            val latitude = event.latitude ?: 0.0
            val longitude = event.longitude ?: 0.0

            val position = LatLng(latitude, longitude)
            val marker = mMap.addMarker(MarkerOptions().position(position).title(event.name))
            marker.tag = events.indexOf(event)

        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.setOnInfoWindowClickListener(this)
        mIsMapReady = true

        reloadMarkers()

    }

    override fun onInfoWindowClick(p0: Marker?) {
        val index = p0?.tag as? Int

        //Controllo che effettivamente ci siano eventi nella lista
        if(events.size > index ?: 0)
        {
            val event = events[index ?: 0]

            //Creo la schermata di dettaglio dell'evento
            val intent = Intent(activity, EventDetailActivity::class.java)

            //Gli passo l'evento selezionato sulla mappa
            intent.putExtra("event", event)

            //Vado alla niova schermata
            startActivity(intent)
        }
    }
}