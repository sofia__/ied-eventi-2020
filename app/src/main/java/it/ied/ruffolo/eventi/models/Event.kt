package it.ied.ruffolo.eventi.models

import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
class Event(

    override var id: Int? = null,
    var creator: User? = null,

    override var name: String? = null,
    override var description: String? = null,
    override var imageUrl: String? = null,
    var date: Date? = null,

    var address: String? = null,
    var latitude: Double? = null,
    var longitude: Double? = null,

    var viewsCount: Int = (1..1000).random(),
    var likesCount: Int = (1..viewsCount).random(),
    var commentsCount: Int = (1..likesCount).random(),

    override var quantity : Int = 0,
    override var price: Double = 0.0

) : Purchasable(id, name, description, imageUrl, quantity, price) {

}