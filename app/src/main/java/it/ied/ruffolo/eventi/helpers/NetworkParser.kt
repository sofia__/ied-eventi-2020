package it.ied.ruffolo.eventi.helpers

import com.android.volley.VolleyError
import it.ied.ruffolo.eventi.models.Event
import it.ied.ruffolo.eventi.models.User
import org.json.JSONObject
import java.text.SimpleDateFormat

class NetworkParser {

    companion object{
        fun parseErrorDescription(error: VolleyError) :String? {
            val response = String(error.networkResponse.data)
            val responseObject = JSONObject(response)

            val errorObject = responseObject.optJSONObject("error")
            return errorObject.optString("description")
        }

        fun parseUser(response: String) : User {
            val responseObject = JSONObject(response)
            val dataObject = responseObject.optJSONObject("data")

            return parseUser(dataObject)

        }

        fun parseUser(dataObject: JSONObject?) : User {
            val user = User()

            user.id = dataObject?.optInt("id_utente")

            user.authToken = dataObject?.optString("auth_token")
            user.email = dataObject?.optString("email")

            user.firstName = dataObject?.optString("nome")
            user.lastName = dataObject?.optString("cognome")
            user.city = dataObject?.optString("citta")
            user.birthDate = dataObject?.optString("data_nascita")

            user.avatarUrl = dataObject?.optString("avatar_url")

            return user
        }

        fun parseEventsList(response: String) : List<Event> {
            val responseObject = JSONObject(response)
            val dataArray = responseObject.optJSONArray("data")

            var eventsList = mutableListOf<Event>()
            val maxCounter = dataArray?.length() ?: 0

            for (counter in 0 until maxCounter) {
                val dataObject = dataArray?.optJSONObject(counter)
                val event = parseEvent(dataObject)
                eventsList.add(event)
            }

            return eventsList
        }

        fun parseEvent(dataObject: JSONObject?) : Event {
            val event = Event()

            event.id = dataObject?.optInt("id")

            event.name = dataObject?.optString("nome")
            event.description = dataObject?.optString("descrizione")
            event.imageUrl = dataObject?.optString("cover_url")

            val stringDate = dataObject?.optString("timestamp") ?: ""
            event.date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(stringDate)

            event.address = dataObject?.optString("indirizzo")
            event.latitude = dataObject?.optDouble("lat")
            event.longitude = dataObject?.optDouble("lng")

            event.viewsCount = dataObject?.optInt("numero_visualizzazioni") ?: 0
            event.likesCount = dataObject?.optInt("numero_like")  ?: 0
            event.commentsCount = dataObject?.optInt("numero_commenti")  ?: 0

            event.price = dataObject?.optDouble("prezzo") ?: 0.0

            val creatorObject = dataObject?.optJSONObject("creatore")
            event.creator = parseUser(creatorObject)

            return event
        }
    }
}