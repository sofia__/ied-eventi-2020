package it.ied.ruffolo.eventi.helpers

import android.app.AlertDialog
import android.content.Context
import android.icu.text.CaseMap
import android.widget.Toast
import java.time.Duration

typealias EmptyCallback = () -> Unit
typealias BooleanCallback = (Boolean) -> Unit

class AlertHelper {
    companion object{

        fun showSimpleAlert (context: Context, title: String, message: String? =null, callback: EmptyCallback? = null){
            val builder = AlertDialog.Builder(context)
            builder.setTitle(title)
            builder.setMessage(message)
            builder.setCancelable(false)

            builder.setPositiveButton("Ok") { dialog, which ->
                callback?.invoke()
            }
            builder.show()
        }




        fun showConfirmationAlert (context: Context, title: String, message: String? =null, callback: BooleanCallback? = null){
            val builder = AlertDialog.Builder(context)
            builder.setTitle(title)
            builder.setMessage(message)
            builder.setCancelable(false)

            builder.setPositiveButton("Si") { dialog, which ->
                callback?.invoke(true)
            }
            builder.setNegativeButton("No") { dialog, which ->
                callback?.invoke(false)
            }
            builder.show()
        }
        fun showToast(context: Context?, message: String?, duration: Int = Toast.LENGTH_SHORT){
            Toast.makeText(context,  message , duration).show()
        }
    }
}