package it.ied.ruffolo.eventi.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class User
    (
    var id: Int? = null,
    var authToken: String? = null,
    var email: String? = null,
    var firstName: String? = null,
    var lastName: String? = null,
    var city: String? = null,
    var birthDate: String? = null,
    var avatarUrl: String? = null

) : Parcelable
{
    //Variabili calcolate a runtime
    val fullName: String
        get() {
            if (firstName != null && lastName != null)
            {
                //Nome + Cognome
                return "$firstName $lastName"
            }
            else if(firstName != null)
            {
                //Nome
                return "$firstName"
            }
            else if(lastName != null)
            {
                //Cognome
                return "$lastName"
            }
            else
            {
                //Nessuno dei due
                return ""
            }

        }
}