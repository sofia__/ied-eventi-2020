package it.ied.ruffolo.eventi.adapters

import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.adapter.FragmentViewHolder
import it.ied.ruffolo.eventi.fragments.EventsListFragment
import it.ied.ruffolo.eventi.fragments.EventsMapFragment
import it.ied.ruffolo.eventi.models.Event

class EventsPagerAdapter(var activity: FragmentActivity) : FragmentStateAdapter(activity) {

    var events: List<Event> = ArrayList()
        set(value) {
           field = value
            //Notifico al viewpager che deve ricaricare i suoi Fragment
            notifyDataSetChanged()

        }

    override fun getItemCount(): Int {
        //lista + mappa
        return 2
    }

    override fun createFragment(position: Int): Fragment {
        if (position == 0){
            //Lista
            val fragment = EventsListFragment()
            fragment.events = this.events
            return fragment
        }
        else{
            //Mappa
            val fragment = EventsMapFragment()
            fragment.events = this.events
            return fragment
        }
    }

    override fun onBindViewHolder(holder: FragmentViewHolder, position: Int, payloads: MutableList<Any> ) {
        super.onBindViewHolder(holder, position, payloads)

        val tag = "f" + holder.itemId
        val fragment = activity.supportFragmentManager.findFragmentByTag(tag)

        if(fragment is EventsListFragment){
            //Lista
            fragment.events = this.events
        }else if (fragment is EventsMapFragment){
            //Mappa
            fragment.events = this.events
        }
    }
}