package it.ied.ruffolo.eventi.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import it.ied.ruffolo.eventi.R
import it.ied.ruffolo.eventi.helpers.LoginHelper

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Controllo se c'è un utente connesso oppure no
        LoginHelper.load(this)


        //In futuro verrà controllato se un utente è loggato
        if(LoginHelper.utenteConnesso != null)
        {
            //Da login alla Lista eventi
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
        }
        else
        {
            //Bisogna anora fare il login
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }

        //Chiuso questa activity di ingresso
        finish()
    }
}