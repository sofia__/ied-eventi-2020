package it.ied.ruffolo.eventi.activities

import android.content.Intent

import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import it.ied.ruffolo.eventi.R

class EventsMapActivity : BaseEventsActivity(), OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener{

    private lateinit var mMap: GoogleMap

    override fun getActivityLayout(): Int {
        return R.layout.activity_events_map
    }

    override fun setupActivityData() {
        super.setupActivityData()

        title = "Map"
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        for (event in events)
        {
            //Aggiungo il marker sulla mappa
            val latitude = event.latitude ?: 0.0
            val longitude = event.longitude ?: 0.0

            val position = LatLng(latitude, longitude)
            val marker = mMap.addMarker(MarkerOptions().position(position).title(event.name))
            marker.tag = events.indexOf(event)

        }

        mMap.setOnInfoWindowClickListener(this)


        // Add a marker in Sydney and move the camera
        //val sydney = LatLng(-34.0, 151.0)
        //mMap.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
        // mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))
    }

    override fun onInfoWindowClick(p0: Marker?) {
        val index = p0?.tag as? Int

        //Controllo che effettivamente ci siano eventi nella lista
        if(events.size > index ?: 0)
        {
            val event = events[index ?: 0]

            //Creo la schermata di dettaglio dell'evento
            val intent = Intent(this, EventDetailActivity::class.java)

            //Gli passo l'evento selezionato sulla mappa
            intent.putExtra("event", event)

            //Vado alla niova schermata
            startActivity(intent)
        }
    }
}