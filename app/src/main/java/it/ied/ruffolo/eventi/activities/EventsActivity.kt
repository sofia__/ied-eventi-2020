package it.ied.ruffolo.eventi.activities

import android.content.Intent
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import it.ied.ruffolo.eventi.*
import it.ied.ruffolo.eventi.adapters.EventsAdapter
import it.ied.ruffolo.eventi.adapters.OnEventClickListener
import it.ied.ruffolo.eventi.helpers.NetworkParser
import it.ied.ruffolo.eventi.models.Event
import kotlinx.android.synthetic.main.activity_events.*

class EventsActivity : BaseEventsActivity(), OnEventClickListener {


    override fun getActivityLayout(): Int {
        return R.layout.activity_events
    }

    override fun setupActivityData() {
        super.setupActivityData()
        recycler_view.layoutManager = LinearLayoutManager(this)
        val adapter = EventsAdapter(events, this)
        adapter.onEventClickListener = this
        recycler_view.adapter = adapter
        title = "Home"

        updateEventsList()

        val url = "http://ied.apptoyou.it/app/meteoroma.php"
        /*
        val request = StringRequest(Request.Method.GET, url,
            { response ->
                Toast.makeText(this, "Meteo di Roma: $response", Toast.LENGTH_SHORT).show()
            },
            { error ->
                Toast.makeText(this, "Errore", Toast.LENGTH_LONG).show()
            })

        val queue = Volley.newRequestQueue(this)
        queue.add(request)

         */
    }
    fun updateEventsList() {
        var url = "http://ied.apptoyou.it/app/api/eventi.php"

        val request = StringRequest(Request.Method.GET, url,
            { response ->
                val eventsList = NetworkParser.parseEventsList(response)

                (recycler_view.adapter as? EventsAdapter)?.setEventsList(eventsList)

                Toast.makeText(this, "Eventi caricati: ${eventsList.size}", Toast.LENGTH_SHORT).show()
            },
            {
                    error ->
                Toast.makeText(this, "Si è verificato un errore", Toast.LENGTH_SHORT).show()
            })

        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun OnEventClick(event: Event) {
        val intent = Intent(this, EventDetailActivity::class.java)

        intent.putExtra("event", event)

        startActivity(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menuitem_shoppingcart, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.button_shoppingcart){
            val intent = Intent(this, ShoppingCartActivity::class.java)
            startActivity(intent)
        }
        return super.onOptionsItemSelected(item)
    }
}