package it.ied.ruffolo.eventi.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import it.ied.ruffolo.eventi.R
import it.ied.ruffolo.eventi.models.Purchasable
import kotlinx.android.synthetic.main.listitem_shoppingcart.view.*
import kotlin.math.roundToInt

class ShoppingCartAdapter (
    var items: MutableList<Purchasable>,
    val context: Context
) : RecyclerView.Adapter<ShoppingCartViewHolder>() {

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShoppingCartViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.listitem_shoppingcart, parent, false)
        return ShoppingCartViewHolder(view)

    }

    override fun onBindViewHolder(holder: ShoppingCartViewHolder, position: Int) {
        val purchasable = items[position]

        holder.textName.text = purchasable.name
        holder.textPrice.text = "${purchasable.price.roundToInt()} €"
    }

}


class ShoppingCartViewHolder (view: View) : RecyclerView.ViewHolder(view){
    val textName = view.text_name
    val textPrice = view.text_price

}