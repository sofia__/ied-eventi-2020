package it.ied.ruffolo.eventi.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import it.ied.ruffolo.eventi.R
import it.ied.ruffolo.eventi.helpers.LoginHelper
import it.ied.ruffolo.eventi.helpers.NetworkParser
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        title = "Accedi"

        button_login.setOnClickListener {
            performLogin()
        }

        //Codice per velocizzare lo sviluppo

    }

    fun performLogin()
    {
        //Indirizzo del servizio web da chiamare
        val url = "http://ied.apptoyou.it/app/login.php"
        val parameters = HashMap<String, String>()

        //Preparo i parametri da passare al servizio web
        parameters["email"] = text_email.text.toString()
        parameters["password"] = text_password.text.toString()

        //Creo la richiesta
        val request = object : StringRequest(
            Method.POST, url,
            { response ->

                //Chiamata andata a buon fine
                LoginHelper.utenteConnesso = NetworkParser.parseUser(response)
                LoginHelper.save(this)

                //Utente loggato apro la homepage
                val intent = Intent(this, HomeActivity::class.java)
                startActivity(intent)
                finish()

            },
            { error ->

                val description = NetworkParser.parseErrorDescription(error)
                Toast.makeText(this, description, Toast.LENGTH_SHORT).show()

            }

        ){
            //Faccio l'override della funzione standard, per utilizzare i miei parametri
            override fun getParams(): MutableMap<String, String>
            {
                return parameters
            }

        }

        //Eseguo la richiesta
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

}