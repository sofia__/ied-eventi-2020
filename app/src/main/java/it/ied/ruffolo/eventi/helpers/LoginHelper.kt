package it.ied.ruffolo.eventi.helpers

import android.content.Context
import com.google.gson.Gson
import it.ied.ruffolo.eventi.models.User

class LoginHelper {

    companion object
    {
        var utenteConnesso: User? = null

        fun load(context: Context)
        {
            val database = context.getSharedPreferences("AppDatabase", Context.MODE_PRIVATE)
            val jsonLoggedUser = database.getString("LoggedUser", null)

            //Converto l'utente salvato sul database da una stringa JSON a un oggetto User
            utenteConnesso = Gson().fromJson<User>(jsonLoggedUser, User::class.java)

        }

        fun save(context: Context)
        {
            //Converto l'utente connesso in una stringa JSON da salvare sul database
            val jsonLoggedUser = Gson().toJson(utenteConnesso)
            val database = context.getSharedPreferences("AppDatabase", Context.MODE_PRIVATE)

            val editor = database.edit()

            editor.putString("LoggedUser", jsonLoggedUser)
            editor.apply()


        }

    }
}