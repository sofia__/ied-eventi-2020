package it.ied.ruffolo.eventi.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import it.ied.ruffolo.eventi.R

class ProfileFragment : BaseFragment() {


    override fun getFragmentLayout(): Int {
        return R.layout.fragment_profile
    }

}