package it.ied.ruffolo.eventi.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import it.ied.ruffolo.eventi.R
import it.ied.ruffolo.eventi.adapters.ShoppingCartAdapter
import it.ied.ruffolo.eventi.helpers.AlertHelper
import it.ied.ruffolo.eventi.helpers.ShoppingCartHelper
import kotlinx.android.synthetic.main.activity_shopping_cart.*
import kotlinx.android.synthetic.main.activity_shopping_cart.recycler_view
import kotlin.math.roundToInt

class ShoppingCartActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shopping_cart)

        title = "Carrello"
        button_clear.text = "SVUOTA CARRELLO"

        button_clear.setOnClickListener{
            showClearCartDialog()
        }
        recycler_view.layoutManager = LinearLayoutManager(this)
        val adapter = ShoppingCartAdapter(ShoppingCartHelper.items, this)
        recycler_view.adapter = adapter

        updateActivityData()
    }

    fun updateActivityData() {
        var totalPrice = 0.0
        for (item in ShoppingCartHelper.items) {
            totalPrice += item.price
        }

        text_total.text = "Il totale del carrello è: ${totalPrice.roundToInt()} €"
        val adapter = recycler_view.adapter

        if (adapter is ShoppingCartAdapter) {
            adapter.items = ShoppingCartHelper.items
            recycler_view.adapter?.notifyDataSetChanged()
        }
    }

    fun showClearCartDialog(){

        AlertHelper.showConfirmationAlert(this,"Conferma operazione", "Sei sicuro di voler svuotare il carrello?"){result ->
            if (result){
                //Svuoto la lista
                ShoppingCartHelper.clearCart()
                updateActivityData()

                AlertHelper.showSimpleAlert(this, "Il carrello ora è vuoto")
            }
        }

    }
}